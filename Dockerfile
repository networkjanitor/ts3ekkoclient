# use debian buster because of python3.6 not being in stretch
FROM debian:buster

# Install all the required packages
RUN apt update && apt -y install \
    pulseaudio-utils \
    python3.6 \
    python3-pip \
    python3-psycopg2 \
    ca-certificates \
    libquazip-dev \
    libxcursor-dev \
    libxcomposite-dev \
    bzip2 \
    consolekit \
    dbus \
    libasound2 \
    libglib2.0-0 \
    libnss3 \
    libqt5core5a libqt5gui5 libqt5network5 libqt5sql5 \
    libxcursor1 \
    pulseaudio \
    xvfb \
    mpv \
    libmpv1

COPY dockerassets/TeamSpeak3-Client-linux_amd64-3.1.3.run /opt/ts3install.run

# switch workdir so that TS3 installs itself into /opt/ directory
WORKDIR /opt/
RUN chmod u+x ./ts3install.run && (sleep 3 && echo "q" && sleep 3 && echo "y") | ./ts3install.run


# Copy pre-prepared ts3 configuration, needs adjustment through preparation 
# scripts regarding used identity/username
COPY dockerassets/.ts3client /root/.ts3client

# Set global python3 version to 3.6 instead of 3.5
# FIXME: venv would be better here, but that would require more work
RUN rm /usr/bin/python3 && ln -s /usr/bin/python3.6 /usr/bin/python3

# Install for the preperation&start script
RUN pip3 install ts3ekkoclient==0.0.9.dev6

# Switch to "home" directoy for ease of use
WORKDIR /root/

ENTRYPOINT ["ts3ekkoentry"]
