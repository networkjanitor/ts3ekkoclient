import unittest
import unittest.mock
import ts3ekkoclient.entrypoint
import subprocess


class EntrypointTestCase(unittest.TestCase):
    def test_start_teamspeak(self):
        testenv = {'testname': 'testprop'}
        ts3_dir = '/opt/TeamSpeak3-Client-linux_amd64'
        ts3_rs = 'ts3client_runscript.sh'
        ts3_reference_path = '/opt/TeamSpeak3-Client-linux_amd64/ts3client_runscript.sh'
        ts3_url = 'ts3server://test.lan/'

        with unittest.mock.patch('subprocess.Popen', unittest.mock.Mock(spec=subprocess.Popen)) as popen_mock:
            ts3ekkoclient.entrypoint.start_teamspeak(testenv, ts3_dir, ts3_rs, ts3_url)

            popen_mock.assert_called_once_with([ts3_reference_path, ts3_url], env=testenv)

    def test_assemble_ts3url(self):
        ts3url = ts3ekkoclient.entrypoint.assemble_ts3serverurl('server.net', 9997, username='Bot',
                                                                server_password='secretserver',
                                                                channel_name='Test Channel', channel_id='5',
                                                                channel_password='secretchannel', token='token')
        self.assertEqual(ts3url, 'ts3server://server.net?port=9997&nickname=Bot&password=secretserver'
                                 '&channel=Test%20Channel&cid=5&channelpassword=secretchannel&token=token')


if __name__ == '__main__':
    unittest.main()
