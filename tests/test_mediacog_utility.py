import unittest
import unittest.mock

import pathlib
from ts3ekkoclient.errors import EkkoInvalidLocalMedia
from ts3ekkoclient.cogs.media import MediaCog

class MediacogUtilityTest(unittest.TestCase):
    """
    Base test case for commonly used methods during the testing of the library methods of the media cog.
    """
    def setUp(self):
        self.pathlib_exists_mock = unittest.mock.Mock(spec=pathlib.Path.exists)

    def test_seconds_formatting(self):
        self.assertEqual(MediaCog.format_seconds(50), '00:50')
        self.assertEqual(MediaCog.format_seconds(60), '01:00')
        self.assertEqual(MediaCog.format_seconds(3600), '1:00:00')

        with self.assertRaises(TypeError):
            MediaCog.format_seconds('a')

    def test_url_bbcode_removal(self):
        self.assertEqual(MediaCog._remove_url_bbcode('[URL]https://youtube.com/watch?v=0xdeedbeef[/URL]'),
                         'https://youtube.com/watch?v=0xdeedbeef')
        self.assertEqual(MediaCog._remove_url_bbcode('[URL][URL]h[/URL]'), 'h')
        self.assertEqual(MediaCog._remove_url_bbcode(['[URL]h[/URL]', '[URL]b[/URL]']), ['h', 'b'])
        self.assertEqual(MediaCog._remove_url_bbcode(['[url]h[/url]', '[URL]b[/URL]']), ['h', 'b'])

    def test_resolve_mediafile_path(self):
        with unittest.mock.patch('pathlib.Path.exists', self.pathlib_exists_mock):
            self.assertEqual(MediaCog.resolve_mediafile_path('/mnt/media', 'testfile.ogg'),
                             '/mnt/media/testfile.ogg')

    def test_resolve_mediafile_path_jailbreak(self):
        """
        Testing if resolved path outside of media directory raises exception
        """
        with unittest.mock.patch('pathlib.Path.exists', self.pathlib_exists_mock):
            with self.assertRaises(EkkoInvalidLocalMedia):
                MediaCog.resolve_mediafile_path('/mnt/media', '../testfile.ogg')

    def test_resolve_mediafile_path_nonexisting(self):
        self.pathlib_exists_mock.return_value = False
        with unittest.mock.patch('pathlib.Path.exists', self.pathlib_exists_mock):
            with self.assertRaises(EkkoInvalidLocalMedia):
                MediaCog.resolve_mediafile_path('/mnt/media', 'testfile.ogg')


if __name__ == '__main__':
    unittest.main()
