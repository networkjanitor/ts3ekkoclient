import unittest
import unittest.mock

from ts3ekkoclient.bot import EkkoBot


class TS3EkkoUtilityTestCase(unittest.TestCase):
    def test_color_message(self):
        self.assertEqual(EkkoBot.color_message('test messsage'),
                         '[color=#1433b1]test messsage[/color]')

    def test_ts3_smilies_removal(self):
        self.assertEqual(EkkoBot.remove_ts3_smilies(' :)  :D  8)  ;)  :(  :C  :0  :/  :x  :P '),
                         ' :\u200B)  :\u200BD  8\u200B)  ;\u200B)  :\u200B(  :\u200BC  :\u200B0  :\u200B/ '
                         ' :\u200Bx  :\u200BP ')

    def test_message_split(self):
        split = EkkoBot.split_message('abcdefghijkl' * 200, part_length=1024,
                                      text_mod=lambda text: f'[pad]{text}[/pad]')
        self.assertEqual(f'[pad]{ "abcdefghijkl" * 84 }abcd[/pad]', split[0])
        self.assertEqual(f'[pad]\nefghijkl{ "abcdefghijkl" * 83 }abcdefgh[/pad]', split[1])
        self.assertEqual(f'[pad]\nijkl{ "abcdefghijkl" * 31 }[/pad]', split[2])
