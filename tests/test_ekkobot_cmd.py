import unittest
import unittest.mock

from ts3ekkoclient.errors import EkkoUnsuitedCommand, EkkoNonexistentGrant, EkkoNonexistentPermissionDoc
from ts3ekkoclient.bot import EkkoBot
from ts3ekkoclient.permission import PermissionManager, PermissionGrant, PermissionServerGroups, PermissionDoc
from ts3ekkoclient.models import Invoker, MediaAlias
from ts3.response import TS3Event
from ts3ekkoutil.envconsts import EkkoPropertyNames as epn


class TS3EkkoCmdBaseTestCase(unittest.TestCase):
    def setUp(self):
        self.invoker = Invoker('z5TJ18g/dtkja4Xbrs9rOny4vl0=', 'Xyoz', 1)
        # Let's create a single, uniform event for all unsuited-tests.
        self.unsuited_ts3_event = self._create_ts3_event(msg=b'!foobar')

        # Create some made up, but relevant args
        args = {
            epn.TS3_CLIENT_APIKEY: 'A',
            epn.TS3_CLIENTQUERY_HOST: 'localhost',
            epn.TS3_CLIENTQUERY_PORT: '12345',
            epn.TS3_USERNAME: 'Test',
            epn.DB_USERNAME: 'u',
            epn.DB_PASSWORD: 'p',
            epn.DB_HOST: 'h',
            epn.DB_DBNAME: 'n'
        }
        # patch startup method to avoid database connection
        with unittest.mock.patch('ts3ekkoclient.bot.startup', unittest.mock.Mock()):
            # avoid adding MediaCog & TextCog to the bot (unrelated to these tests)
            with unittest.mock.patch('ts3ekkoclient.bot.EkkoBot.add_cog', unittest.mock.Mock()):
                with unittest.mock.patch('ts3ekkoclient.bot.MediaCog', unittest.mock.Mock()):
                    with unittest.mock.patch('ts3ekkoclient.bot.TextCog', unittest.mock.Mock()):
                        # avoid creating actual ts3clientquery connections
                        with unittest.mock.patch('ts3ekkoclient.ts3ekko.TS3Ekko.create_connection',
                                                 unittest.mock.Mock()):
                            # Create the to be tested erkko bot
                            self.ekko_bot = EkkoBot(args)
                            self.ekko_bot.can = unittest.mock.Mock(spec=self.ekko_bot.can)
                            self.ekko_bot.reply = unittest.mock.Mock(spec=self.ekko_bot.reply)
                            self.ekko_bot.permission_manager = unittest.mock.Mock(spec=PermissionManager)

    def _create_ts3_event(self, msg=b'\\s', event_type=b'notifytextmessage', schandlerid=b'1', targetmode=b'2',
                          invokerid=b'1', invokername=b'Xyoz', invokeruid=b'z5TJ18g\\/dtkja4Xbrs9rOny4vl0='):
        creator_seq = b'%s schandlerid=%s targetmode=%s msg=%s invokerid=%s invokername=%s invokeruid=%s\n\r' % \
                      (event_type, schandlerid, targetmode, msg, invokerid, invokername, invokeruid)
        return TS3Event(creator_seq)


class TS3EkkoCoreCommandsTestCase(TS3EkkoCmdBaseTestCase):
    def test_relay_spawn(self):
        ts3event = self._create_ts3_event(b'!spawn', invokerid=b'123')
        self.ekko_bot.args[epn.EKKO_MANAGE_PORT] = '8080'
        self.ekko_bot.args[epn.EKKO_MANAGE_SERVER] = '1.2.3.4'
        self.ekko_bot.ts3conn.cid_from_clid = unittest.mock.Mock(return_value=3)
        with unittest.mock.patch('requests.get') as requests_get_mock:
            self.ekko_bot.relay_spawn(ts3event)
            self.ekko_bot.ts3conn.cid_from_clid.assert_called_once_with('123')
            requests_get_mock.assert_called_once_with('http://1.2.3.4:8080/cmd/spawn/3')

    def test_relay_spawn_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.relay_spawn(self.unsuited_ts3_event)

    def test_relay_despawn(self):
        ts3event = self._create_ts3_event(b'!despawn')
        self.ekko_bot.args[epn.EKKO_MANAGE_PORT] = '8080'
        self.ekko_bot.args[epn.EKKO_MANAGE_SERVER] = '1.2.3.4'
        self.ekko_bot.args[epn.EKKO_NODE_ID] = 5
        self.ekko_bot.ts3conn.cid_from_clid = unittest.mock.Mock(return_value=3)
        with unittest.mock.patch('requests.get') as requests_get_mock:
            with self.assertRaises(SystemExit):
                self.ekko_bot.relay_despawn(ts3event)
            requests_get_mock.assert_called_once_with('http://1.2.3.4:8080/cmd/despawn/5')

    def test_relay_despawn_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.relay_despawn(self.unsuited_ts3_event)

    def test_join(self):
        ts3event = self._create_ts3_event(b'!join')
        self.ekko_bot.ts3conn.cid_from_clid = unittest.mock.Mock(return_value=3)
        self.ekko_bot.ts3conn.whoami = unittest.mock.Mock(return_value=[{'clid': 42}])
        self.ekko_bot.cmd_join(ts3event)
        self.ekko_bot.ts3conn.clientmove.assert_called_once_with(cid=3, clid=42)

    def test_join_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_join(self.unsuited_ts3_event)

    def test_name(self):
        ts3event = self._create_ts3_event(b'!name\\sNot\\sso\\sEkko\\sBot!')
        self.ekko_bot.cmd_name(ts3event)
        self.ekko_bot.ts3conn.clientupdate.assert_called_once_with(client_nickname='Not so Ekko Bot!')

    def test_name_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_name(self.unsuited_ts3_event)

    def test_whoami(self):
        ts3event = self._create_ts3_event(b'!whoami')
        self.ekko_bot.ts3conn.clientvariable = unittest.mock.Mock(return_value=[{'client_servergroups': '1,2,3',
                                                                                 'client_channel_group_id': '4'}])
        self.ekko_bot.cmd_whoami(ts3event)
        self.ekko_bot.reply.assert_called_once_with('username: Xyoz | server groups: [1, 2, 3] | channel group: 4 |'
                                                    ' unique id: z5TJ18g/dtkja4Xbrs9rOny4vl0=', ts3event)

    def test_whoami_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_whoami(self.unsuited_ts3_event)

    def test_help(self):
        ts3event = self._create_ts3_event(b'!help')
        self.ekko_bot.cmd_help(ts3event)
        self.ekko_bot.reply.assert_called_once_with('List of all available command sets: \n'
                                                    '    !despawn\n'
                                                    '    !join\n'
                                                    '    !name\n'
                                                    '    !permission\n'
                                                    '    !spawn\n'
                                                    '    !whoami\n'
                                                    ' To receive more help about a specific command, use the `-h` or'
                                                    ' `--help` flag when calling a command (e.g. !skip --help)',
                                                    ts3event)

    def test_help_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_help(self.unsuited_ts3_event)


class TS3ekkoUtilityTestCase(TS3EkkoCmdBaseTestCase):
    def test_permission_fallback(self):
        ts3event = self._create_ts3_event(b'!permission')
        self.ekko_bot.cmd_permission(ts3event)
        self.ekko_bot.reply.assert_called_once_with('usage: !permission [ add | get | info | delete ]', ts3event)

    def test_permission_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_permission(self.unsuited_ts3_event)

    def test_permission_add(self):
        ts3event = self._create_ts3_event(
            b'!permission\\sadd\\s-s\\s42\\s-c\\s7\\s-i\\sveryuniqueidentity\\s-d\\scontrol.spawn')
        self.ekko_bot.cmd_permission_add(ts3event)
        add_call_param = self.ekko_bot.permission_manager.add_grant.mock_calls[0][1]
        # permission name
        self.assertEqual(add_call_param[0], 'control.spawn')
        # invokerctx
        self.assertEqual(add_call_param[1].channel_group, 7)
        self.assertEqual(add_call_param[1].server_groups, [42])
        self.assertEqual(add_call_param[1].unique_id, 'veryuniqueidentity')
        # if deny or not
        self.assertEqual(add_call_param[2], True)

    def test_permission_add_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_permission_add(self.unsuited_ts3_event)

    def test_permission_get(self):
        ts3event = self._create_ts3_event(b'!permission\\sget\\scontrol.spawn')
        cmp_grant1 = PermissionGrant(id=2, name='other.whoami', channel_group=6, deny=True,
                                     unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0=')
        cmp_sg1 = PermissionServerGroups(id=3, server_group_id=40, permission_grant=cmp_grant1)
        cmp_sg2 = PermissionServerGroups(id=4, server_group_id=5, permission_grant=cmp_grant1)

        cmp_grant2 = PermissionGrant(id=3, name='other.whoami', channel_group=2, deny=False)

        self.ekko_bot.permission_manager.get_grant = unittest.mock.Mock(return_value=[cmp_grant1, cmp_grant2])
        self.ekko_bot.resolve_servergroups = unittest.mock.Mock(
            return_value={40: 'ServergroupName40', 5: 'ServergroupName5'})
        self.ekko_bot.cmd_permission_get(ts3event)
        self.ekko_bot.reply.assert_called_once_with('Allowed entities: \n(id=3): Channel Group: 2, Server Groups: any, '
                                                    '\n Forbidden entities: \n(id=2): Channel Group: 6, Server Groups: '
                                                    'ServergroupName40,ServergroupName5, '
                                                    'Unique ID: z5TJ18g/dtkja4Xbrs9rOny4vl0=', ts3event)

    def test_permission_get_nonexistant(self):
        ts3event = self._create_ts3_event(b'!permission\\sget\\scontrol.spawn')
        self.ekko_bot.permission_manager.get_grant = unittest.mock.Mock(return_value=[])
        self.ekko_bot.resolve_servergroups = unittest.mock.Mock(return_value={})
        self.ekko_bot.cmd_permission_get(ts3event)
        self.ekko_bot.reply.assert_called_once_with('no grants for this permission found.', ts3event)

    def test_permission_get_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_permission_get(self.unsuited_ts3_event)

    def test_permission_info(self):
        ts3event = self._create_ts3_event(b'!permission\\sinfo\\scontrol.spawn')
        cmp_grant_info = PermissionDoc(name='control.spawn',
                                       short_description='Allows to spawn new instances of the bot on the same server',
                                       long_description='If the invoker has this permission, they are allowed to use '
                                                        'the !spawn command.')

        self.ekko_bot.permission_manager.get_grant_info = unittest.mock.Mock(return_value=cmp_grant_info)
        self.ekko_bot.cmd_permission_info(ts3event)
        self.ekko_bot.reply.assert_called_once_with('\npermission: control.spawn\n\n'
                                                    'Allows to spawn new instances of the bot on the same server\n\n'
                                                    'If the invoker has this permission, they are allowed to use the '
                                                    '!spawn command.', ts3event)

    def test_permission_info_nonexistant(self):
        ts3event = self._create_ts3_event(b'!permission\\sinfo\\scontrol.spawn')
        self.ekko_bot.permission_manager.get_grant_info = \
            unittest.mock.Mock(side_effect=EkkoNonexistentPermissionDoc('control.spawn'))
        self.ekko_bot.cmd_permission_info(ts3event)
        self.ekko_bot.reply.assert_called_once_with("There is no documentation for the permission 'control.spawn' "
                                                    "available.", ts3event)

    def test_permission_info_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_permission_info(self.unsuited_ts3_event)

    def test_permission_delete(self):
        ts3event = self._create_ts3_event(b'!permission\\sdelete\\s2')
        cmp_grant1 = PermissionGrant(id=2, name='other.whoami', channel_group=6, deny=True,
                                     unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0=')
        cmp_sg1 = PermissionServerGroups(id=3, server_group_id=40, permission_grant=cmp_grant1)
        cmp_sg2 = PermissionServerGroups(id=4, server_group_id=5, permission_grant=cmp_grant1)
        self.ekko_bot.permission_manager.delete_grant = unittest.mock.Mock(return_value=cmp_grant1)
        self.ekko_bot.cmd_permission_delete(ts3event)
        self.ekko_bot.reply.assert_called_once_with('grant deleted! (<Grant deny=True, name=other.whoami '
                                                    'channel_group=6, unique_id=z5TJ18g/dtkja4Xbrs9rOny4vl0=, '
                                                    'server_groups={40, 5}>)', ts3event)

    def test_permission_delete_nonexistant(self):
        ts3event = self._create_ts3_event(b'!permission\\sdelete\\s2')
        self.ekko_bot.permission_manager.delete_grant = unittest.mock.Mock(side_effect=EkkoNonexistentGrant(2))
        self.ekko_bot.cmd_permission_delete(ts3event)
        self.ekko_bot.reply.assert_called_once_with('There is no permission grant with the id 2.', ts3event)

    def test_permission_delete_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.ekko_bot.cmd_permission_delete(self.unsuited_ts3_event)


if __name__ == '__main__':
    unittest.main()
