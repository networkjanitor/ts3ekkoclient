import unittest
import unittest.mock
import mpv

from alchemy_mock.mocking import UnifiedAlchemyMagicMock, mock

from ts3ekkoclient.errors import EkkoNonexistentAlias
from ts3ekkoclient.permission import PermissionManager, PermissionDenied, PermissionGrant, PermissionDuplicate, \
    PermissionServerGroups, InvokerCtx
from ts3ekkoclient.bot import EkkoBot
from ts3ekkoclient.models import Invoker, MediaAlias
from ts3ekkoutil.envconsts import EkkoPropertyNames as epn


class PermissionManagerLibBaseCase(unittest.TestCase):
    """
    Base test case for commonly used methods during the testing of the library methods of the permission manager.
    """

    def custom_session_setup(self, dbsession):
        # Mock the parent EkkoBot of the text cog
        self.ekko_bot = unittest.mock.Mock(spec=EkkoBot)
        # Create some made up, but relevant args
        args = {
            epn.EKKO_MEDIA_DIRECTORY: '/mnt/media/',
            epn.COG_MEDIA_ALIAS_PREFIX: '\$',
            epn.COG_MEDIA_VOLUME_MAX: 100,
            epn.COG_MEDIA_VOLUME_MODIFIER: 0.2
        }
        # Create the to be tested permission manager
        with unittest.mock.patch('mpv.MPV', unittest.mock.Mock(spec=mpv.MPV)):
            self.permission_manager = PermissionManager(self.ekko_bot, dbsession)


class PermissionManagerLibTestDuplicate(PermissionManagerLibBaseCase):
    def test_permission_find_duplicate(self):
        """
        Test if the _find_duplicate raises PermissionDuplicate on finding a duplicate.
        :return: 
        """
        # Create on grant with two server groups.
        cmp_grant = PermissionGrant(id=1, name='other.whoami', channel_group=6, deny=False,
                                    unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0=')
        cmp_sg1 = PermissionServerGroups(id=1, server_group_id=40, permission_grant=cmp_grant)
        cmp_sg2 = PermissionServerGroups(id=2, server_group_id=5, permission_grant=cmp_grant)

        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(PermissionGrant),
                 mock.call.filter_by(name=cmp_grant.name, deny=cmp_grant.deny,
                                     channel_group=cmp_grant.channel_group,
                                     unique_id=cmp_grant.unique_id)],
                [cmp_grant]
            )
        ])
        self.custom_session_setup(session)
        # Check if it is detected as a duplicate of itself.
        with self.assertRaises(PermissionDuplicate):
            self.permission_manager._find_duplicate(cmp_grant)

    def test_permission_find_duplicate_diff(self):
        """
        Test if the _find_duplicate can differentiate between different server groups of grants.
        :return: 
        """
        # Create on grant with two server groups.
        cmp_grant = PermissionGrant(id=1, name='other.whoami', channel_group=6, deny=False,
                                    unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0=')
        cmp_sg1 = PermissionServerGroups(id=1, server_group_id=40, permission_grant=cmp_grant)
        cmp_sg2 = PermissionServerGroups(id=2, server_group_id=5, permission_grant=cmp_grant)

        # Create another grant which only differs in the assigned server groups and ids.
        cmp_diff_grant = PermissionGrant(id=2, name='other.whoami', channel_group=6, deny=False,
                                         unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0=')
        cmp_diff_sg1 = PermissionServerGroups(id=3, server_group_id=40, permission_grant=cmp_diff_grant)

        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(PermissionGrant),
                 mock.call.filter_by(name=cmp_grant.name, deny=cmp_grant.deny,
                                     channel_group=cmp_grant.channel_group,
                                     unique_id=cmp_grant.unique_id)],
                [cmp_diff_grant]
            )
        ])
        self.custom_session_setup(session)
        self.permission_manager._find_duplicate(cmp_grant)


class PermissionManagerLibTestQueryGrants(PermissionManagerLibBaseCase):
    def setUp(self):
        # Create on grant with two server groups.
        cmp_grant = PermissionGrant(id=1, name='other.whoami', channel_group=6, deny=False,
                                    unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0=')
        cmp_sg1 = PermissionServerGroups(id=1, server_group_id=40, permission_grant=cmp_grant)
        cmp_sg2 = PermissionServerGroups(id=2, server_group_id=5, permission_grant=cmp_grant)

        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(PermissionGrant),
                 mock.call.filter_by(name=cmp_grant.name, deny=False)],
                [cmp_grant]
            )
        ])
        self.custom_session_setup(session)

    def test_permission_query_grant(self):
        """
        Check if _query_grant properly allows action on no deny.
        """
        self.permission_manager._query_grants('other.whoami', InvokerCtx(channel_group=6, server_groups=[40, 5],
                                                                         unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0='))

    def test_permission_query_grant_mismatch(self):
        """
        Check if _query_grant properly denies action on no deny when no matching grant can be found.
        """
        # channel group differs
        with self.assertRaises(PermissionDenied):
            self.permission_manager._query_grants('other.whoami', InvokerCtx(channel_group=1, server_groups=[40, 5],
                                                                          unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0='))
        # server groups differ
        with self.assertRaises(PermissionDenied):
            self.permission_manager._query_grants('other.whoami', InvokerCtx(channel_group=6, server_groups=[1, 2],
                                                                             unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0='))
        # unique id differs
        with self.assertRaises(PermissionDenied):
            self.permission_manager._query_grants('other.whoami', InvokerCtx(channel_group=6, server_groups=[40, 5],
                                                                             unique_id='test'))

class PermissionManagerLibTestQueryDenies(PermissionManagerLibBaseCase):
    def setUp(self):
        # Create on deny grant with two server groups.
        cmp_deny_grant = PermissionGrant(id=2, name='other.whoami', channel_group=6, deny=True,
                                         unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0=')
        cmp_deny_sg1 = PermissionServerGroups(id=3, server_group_id=40, permission_grant=cmp_deny_grant)
        cmp_deny_sg2 = PermissionServerGroups(id=4, server_group_id=5, permission_grant=cmp_deny_grant)

        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(PermissionGrant),
                 mock.call.filter_by(name=cmp_deny_grant.name, deny=True)],
                [cmp_deny_grant]
            )
        ])
        self.custom_session_setup(session)

    def test_permission_query_denies(self):
        """
        Check if _query_denies properly denies action on deny.
        """

        # channel group differs
        with self.assertRaises(PermissionDenied):
            self.permission_manager._query_denies('other.whoami', InvokerCtx(channel_group=6, server_groups=[40, 5],
                                                                             unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0='))

    def test_permission_query_denies_mismatch(self):
        """
        Check if _query_denies properly allows action when no matching deny grant can be found.
        """
        # channel group differs
        self.permission_manager._query_denies('other.whoami', InvokerCtx(channel_group=1, server_groups=[40, 5],
                                                                          unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0='))
        # server groups differ
        self.permission_manager._query_denies('other.whoami', InvokerCtx(channel_group=6, server_groups=[1, 2],
                                                                             unique_id='z5TJ18g/dtkja4Xbrs9rOny4vl0='))
        # unique id differs
        self.permission_manager._query_denies('other.whoami', InvokerCtx(channel_group=6, server_groups=[40, 5],
                                                                             unique_id='test'))

if __name__ == '__main__':
    unittest.main()
