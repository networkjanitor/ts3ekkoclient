import unittest
import unittest.mock
import datetime

from ts3.response import TS3Event
from alchemy_mock.mocking import UnifiedAlchemyMagicMock, mock

from ts3ekkoclient.errors import EkkoUnsuitedCommand, EkkoNonexistentAlias, EkkoParsingError, EkkoArgparserMessage
from ts3ekkoclient.cogs.text import TextCog
from ts3ekkoclient.bot import EkkoBot
from ts3ekkoclient.models import Invoker, TextAlias


class TextcogTextaliasLibTestCase(unittest.TestCase):
    def setUp(self):
        self.invoker = Invoker('z5TJ18g/dtkja4Xbrs9rOny4vl0=', 'Xyoz', 1)

        self.temporary_aliase = [
            TextAlias(alias='test1', value='testvalue1', timestamp=datetime.datetime(2017, 12, 11, 3, 12, 5),
                      invoker=self.invoker),
            TextAlias(alias='test2', value='testvalue2', timestamp=datetime.datetime(2017, 12, 10, 3, 12, 5),
                      invoker=self.invoker),
            TextAlias(alias='test3', value='testvalue3', timestamp=datetime.datetime(2017, 12, 9, 3, 12, 5),
                      invoker=self.invoker)
        ]

        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(TextAlias),
                 mock.call.order_by(TextAlias.id.desc())],
                self.temporary_aliase
            )
        ])

        self.ekko_bot = unittest.mock.Mock(spec=EkkoBot)
        self.text_cog = TextCog(self.ekko_bot, None, session)

        self.text_cog.temporary_aliase = self.temporary_aliase

    def _create_ts3_event(self, msg=b'\\s', event_type=b'notifytextmessage', schandlerid=b'1', targetmode=b'2',
                          invokerid=b'1', invokername=b'Xyoz', invokeruid=b'z5TJ18g\\/dtkja4Xbrs9rOny4vl0='):
        """
        Shortcut to create TS3Events (mostly notifytextmessage) for testing purposes.

        For parameter documentation check TS3ClientQuery docs.

        :param event_type: ts3 event type (e.g. notifytextmessage)
        :return: TS3Event
        """
        creator_seq = b'%s schandlerid=%s targetmode=%s msg=%s invokerid=%s invokername=%s invokeruid=%s\n\r' % \
                      (event_type, schandlerid, targetmode, msg, invokerid, invokername, invokeruid)
        return TS3Event(creator_seq)


class TextcogTextaliasLibTest(TextcogTextaliasLibTestCase):
    def testAddAliasPermanent(self):
        self.text_cog._add_alias('test', 'testvalue', self.invoker, True)
        add_call_param = self.text_cog.dbsession.add.mock_calls[0][1][0]
        self.assertTrue(add_call_param.alias == 'test' and add_call_param.value == 'testvalue' and
                        add_call_param.invoker == self.invoker)

    def testAddAliasTemporary(self):
        self.text_cog.temporary_aliase = []
        self.text_cog._add_alias('test', 'testvalue', self.invoker)
        temp_alias = self.text_cog.temporary_aliase[0]
        self.assertTrue(temp_alias.alias == 'test' and temp_alias.value == 'testvalue' and
                        temp_alias.invoker == self.invoker)


class TextcogTextaliasListLibTest(TextcogTextaliasLibTestCase):
    def test_list_temporary_alias(self):
        listing = self.text_cog._list_temporary_alias()
        self.assertEqual(listing, 'temporary available alias:\ntest1\ntest2\ntest3')

    def test_list_temporary_alias_empty(self):
        self.text_cog.temporary_aliase = []
        listing = self.text_cog._list_temporary_alias()
        self.assertEqual(listing, 'temporary available alias:\nno temporary alias available!')

    def test_list_permanent_alias(self):
        listing = self.text_cog._list_permanent_alias()
        self.assertEqual(listing, 'permanent available alias:\n'
                                  'Keyword: test1, set by Xyoz, set on 2017-12-11 03:12:05\n'
                                  'Keyword: test2, set by Xyoz, set on 2017-12-10 03:12:05\n'
                                  'Keyword: test3, set by Xyoz, set on 2017-12-09 03:12:05')

    def test_list_permanent_alias_empty(self):
        self.text_cog.dbsession = unittest.mock.Mock()
        self.text_cog.dbsession.query.return_value \
            .order_by.return_value \
            .all.return_value = []
        listing = self.text_cog._list_permanent_alias()
        self.assertEqual(listing, 'permanent available alias:\nno permanent alias available!')


class TextcogTextaliasDeleteLibTest(TextcogTextaliasLibTestCase):
    def test_delete_alias_temporary(self):
        self.text_cog._delete_temporary_alias = unittest.mock.Mock(spec=self.text_cog._delete_temporary_alias)
        self.text_cog.delete_alias('test1', self.invoker, False)
        self.text_cog._delete_temporary_alias.assert_called_once_with('test1')

    def test_delete_alias_permanent(self):
        self.text_cog._delete_permanent_alias = unittest.mock.Mock(spec=self.text_cog._delete_permanent_alias)
        self.text_cog.delete_alias('test1', self.invoker, True)
        self.text_cog._delete_permanent_alias.assert_called_once_with('test1', self.invoker)

    def test_delete_temporary_alias(self):
        self.text_cog._delete_temporary_alias('test1')
        for ta in self.text_cog.temporary_aliase:
            self.assertNotEqual(ta.alias, 'test1')

    def test_delete_permanent_alias(self):
        self.text_cog._add_alias = unittest.mock.Mock(spec=self.text_cog._add_alias)
        self.text_cog._find_permanent_alias = unittest.mock.Mock(spec=self.text_cog._find_permanent_alias)
        self.text_cog._delete_permanent_alias('test1', self.invoker)
        self.text_cog._add_alias.assert_called_once_with('test1', None, self.invoker, True)

    def test_delete_permanent_alias_nonexistant(self):
        self.text_cog._add_alias = unittest.mock.Mock(spec=self.text_cog._add_alias)
        self.text_cog._find_permanent_alias = unittest.mock.Mock(spec=self.text_cog._find_permanent_alias,
                                                                 side_effect=EkkoNonexistentAlias('test1'))
        self.text_cog._delete_permanent_alias('test1', self.invoker)
        self.assertFalse(self.text_cog._add_alias.called)


class TextcogTextaliasFindAliasLibTest(TextcogTextaliasLibTestCase):
    def test_find_alias_temporary(self):
        self.text_cog._find_temporary_alias = unittest.mock.Mock(spec=self.text_cog._find_temporary_alias,
                                                                 return_value=self.temporary_aliase[0])
        self.text_cog._find_permanent_alias = unittest.mock.Mock(spec=self.text_cog._find_permanent_alias)
        result = self.text_cog.find_alias('test1', permanent=False)
        self.assertEqual(result, self.temporary_aliase[0])
        self.assertFalse(self.text_cog._find_permanent_alias.called)

    def test_find_alias_permanent(self):
        self.text_cog._find_temporary_alias = unittest.mock.Mock(spec=self.text_cog._find_temporary_alias)
        self.text_cog._find_permanent_alias = unittest.mock.Mock(spec=self.text_cog._find_permanent_alias,
                                                                 return_value=self.temporary_aliase[0])
        result = self.text_cog.find_alias('test1', permanent=True)
        self.assertEqual(result, self.temporary_aliase[0])
        self.assertFalse(self.text_cog._find_temporary_alias.called)

    def test_find_alias_temporary_after_permanent(self):
        self.text_cog._find_temporary_alias = unittest.mock.Mock(spec=self.text_cog._find_temporary_alias,
                                                                 return_value=self.temporary_aliase[0])
        self.text_cog._find_permanent_alias = unittest.mock.Mock(spec=self.text_cog._find_permanent_alias,
                                                                 side_effect=EkkoNonexistentAlias('test1'))
        result = self.text_cog.find_alias('test1', permanent=None)
        self.text_cog._find_permanent_alias.assert_called_once_with('test1')
        self.text_cog._find_temporary_alias.assert_called_once_with('test1')
        self.assertEqual(result, self.temporary_aliase[0])
