import unittest
import unittest.mock
import mpv

from ts3ekkoclient.errors import EkkoUnsuitedCommand, EkkoNonexistentAlias, EkkoParsingError
from ts3ekkoclient.cogs.media import MediaCog
from ts3ekkoclient.bot import EkkoBot
from ts3ekkoclient.models import Invoker, MediaAlias
from ts3.response import TS3Event
from ts3ekkoutil.envconsts import EkkoPropertyNames as epn


class MediacogTestCase(unittest.TestCase):
    def setUp(self):
        self.invoker = Invoker('z5TJ18g/dtkja4Xbrs9rOny4vl0=','Xyoz', 1)
        # Mock the parent EkkoBot of the text cog
        self.ekko_bot = unittest.mock.Mock(spec=EkkoBot)
        # Invoker determination returns prepared invoker
        self.ekko_bot.determine_invoker.return_value = self.invoker
        # Running with non-mocked command suitability
        self.ekko_bot.check_cmd_suitability = EkkoBot.check_cmd_suitability
        # Running with non-mocked parsing method (needs to be bound)
        self.ekko_bot.parse = EkkoBot.parse.__get__(self.ekko_bot, EkkoBot)
        # Let's create a single, uniform event for all unsuited-tests.
        self.unsuited_ts3_event = self._create_ts3_event(msg=b'!foobar')

        # We dont want to have db access
        dbsession = unittest.mock.Mock()
        # Create some made up, but relevant args
        args = {
            epn.EKKO_MEDIA_DIRECTORY: '/mnt/media/',
            epn.COG_MEDIA_ALIAS_PREFIX: '\$',
            epn.COG_MEDIA_VOLUME_MAX: 100,
            epn.COG_MEDIA_VOLUME_MODIFIER: 0.2
        }
        # Create the to be tested media cog
        with unittest.mock.patch('mpv.MPV', unittest.mock.Mock(spec=mpv.MPV)):
            self.media_cog = MediaCog(self.ekko_bot, args, dbsession)

    def _create_ts3_event(self, msg=b'\\s', event_type=b'notifytextmessage', schandlerid=b'1', targetmode=b'2',
                          invokerid=b'1', invokername=b'Xyoz', invokeruid=b'z5TJ18g\\/dtkja4Xbrs9rOny4vl0='):
        creator_seq = b'%s schandlerid=%s targetmode=%s msg=%s invokerid=%s invokername=%s invokeruid=%s\n\r' % \
                      (event_type, schandlerid, targetmode, msg, invokerid, invokername, invokeruid)
        return TS3Event(creator_seq)


class MediacogTestVolume(MediacogTestCase):
    def setUp(self):
        super().setUp()
        self.reset_volume_ref = self.media_cog.mpv.volume

    def test_volume_help(self):
        ts3_event = self._create_ts3_event(msg=b'!volume\\s-h')
        self.media_cog.cmd_volume(ts3_event)
        self.ekko_bot.reply.assert_called_once_with('usage: !volume [new volume] '
                                                    '\n    get current volume: !volume'
                                                    '\n    set new volume: !volume 100', ts3_event)

    def test_volume_set(self):
        ts3_event = self._create_ts3_event(msg=b'!volume\\s50')
        self.media_cog.cmd_volume_set(ts3_event)
        self.assertEqual(self.media_cog.mpv.volume, 50 * self.media_cog._volume_modifier)
        self.ekko_bot.reply.assert_called_once_with('Volume set to 50.0.', ts3_event)

    def test_volume_set_alphanumeric(self):
        ts3_event = self._create_ts3_event(msg=b'!volume\\s50a')
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_volume_set(ts3_event)

    def test_volume_set_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_volume_set(self.unsuited_ts3_event)

    def test_volume_reset(self):
        ts3_event = self._create_ts3_event(msg=b'!volume\\sreset')
        self.media_cog.cmd_volumereset(ts3_event)
        self.assertEqual(self.media_cog.mpv.volume, self.reset_volume_ref)
        self.ekko_bot.reply.assert_called_once_with('Volume reset.', ts3_event)

    def test_volume_reset_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_volumereset(self.unsuited_ts3_event)

    def test_volume_get(self):
        ts3_event = self._create_ts3_event(msg=b'!volume')
        self.media_cog.cmd_volume_get(ts3_event)
        self.assertEqual(self.media_cog.mpv.volume, self.reset_volume_ref)
        self.ekko_bot.reply.assert_called_once_with('Current volume: 50.0', ts3_event)

    def test_volume_get_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_volume_get(self.unsuited_ts3_event)


class MediacogMediaaliasTest(MediacogTestCase):
    def setUp(self):
        super().setUp()

    def test_mediaalias_help(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias')
        self.media_cog.cmd_mediaalias(ts3_event)
        self.ekko_bot.reply.assert_called_once_with('usage: !mediaalias [ get | set | append | delete ]', ts3_event)

    def test_mediaalias_set(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sset\\stest0\\s[URL]https://youtu.be/0xdeadbeef[/URL]')
        self.media_cog._set_mediaalias = unittest.mock.Mock(spec=self.media_cog._set_mediaalias)
        self.media_cog.cmd_mediaalias_set(ts3_event)
        self.media_cog._set_mediaalias.assert_called_once_with('test0', '[URL]https://youtu.be/0xdeadbeef[/URL]',
                                                               self.invoker)

    def test_mediaalias_set_empty(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sset\\stest0')
        with self.assertRaises(EkkoParsingError):
            self.media_cog.cmd_mediaalias_set(ts3_event)

    def test_mediaalias_set_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_mediaalias_set(self.unsuited_ts3_event)

    def test_mediaalias_set_multiple(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sset\\stest0\\s[URL]https://youtu.be/0xdeadbeef'
                                               b'[/URL]\\s\\s\\shttps://youtube.com/watch?v=0xc0ffee\\smedia.aac')
        self.media_cog._set_mediaalias = unittest.mock.Mock(spec=self.media_cog._set_mediaalias)
        self.media_cog.cmd_mediaalias_set(ts3_event)
        self.media_cog._set_mediaalias \
            .assert_called_once_with('test0', '[URL]https://youtu.be/0xdeadbeef[/URL] '
                                              'https://youtube.com/watch?v=0xc0ffee media.aac', self.invoker)

    def test_mediaalias_append(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sappend\\stest0\\shttps://youtu.be/0xdeadbeef')
        self.media_cog._append_mediaalias = unittest.mock.Mock(spec=self.media_cog._append_mediaalias)
        self.media_cog.cmd_mediaalias_append(ts3_event)
        self.media_cog._append_mediaalias.assert_called_once_with('test0', 'https://youtu.be/0xdeadbeef',
                                                                  self.invoker)

    def test_mediaalias_append_empty(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sappend\\stest0')
        with self.assertRaises(EkkoParsingError):
            self.media_cog.cmd_mediaalias_append(ts3_event)

    def test_mediaalias_append_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_mediaalias_append(self.unsuited_ts3_event)

    def test_mediaalias_get(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sget\\stestalias')
        self.media_cog._get_mediaalias = unittest.mock.Mock(spec=self.media_cog._get_mediaalias)
        ret_ma = MediaAlias(invoker=self.invoker)
        ret_ma.value = '[url]https://youtube.com/watch?v=0xc0ffee[/url]'
        self.media_cog._get_mediaalias.return_value = ret_ma
        self.media_cog.cmd_mediaalias_get(ts3_event)
        self.media_cog.ekko_bot.reply.assert_called_once_with(f'MediaAlias "testalias" (created by "Xyoz"/'
                                                              f'"z5TJ18g/dtkja4Xbrs9rOny4vl0=") stands for: '
                                                              f'[url]https://youtube.com/watch?v=0xc0ffee[/url]',
                                                              ts3_event)

    def test_mediaalias_get_empty(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sget\\s')
        with self.assertRaises(EkkoParsingError):
            self.media_cog.cmd_mediaalias_get(ts3_event)

    def test_mediaalias_get_nonexistent(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sget\\stestalias1')
        self.media_cog._get_mediaalias = unittest.mock.Mock(spec=self.media_cog._get_mediaalias)
        self.media_cog._get_mediaalias.side_effect = EkkoNonexistentAlias('testalias1')
        self.media_cog.cmd_mediaalias_get(ts3_event)
        self.media_cog.ekko_bot.reply.assert_called_once_with('no such alias available.', ts3_event)

    def test_mediaalias_get_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_mediaalias_get(self.unsuited_ts3_event)

    def test_mediaalias_delete(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sdelete\\stestalias1')
        self.media_cog._delete_mediaalias = unittest.mock.Mock(spec=self.media_cog._delete_mediaalias)
        self.media_cog.cmd_mediaalias_delete(ts3_event)
        self.media_cog.ekko_bot.reply.assert_called_once_with('MediaAlias "testalias1" has been deleted.', ts3_event)

    def test_mediaalias_delete_empty(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sdelete\\s')
        with self.assertRaises(EkkoParsingError):
            self.media_cog.cmd_mediaalias_delete(ts3_event)

    def test_mediaalias_delete_nonexistent(self):
        ts3_event = self._create_ts3_event(msg=b'!mediaalias\\sdelete\\stestalias1')
        self.media_cog._delete_mediaalias = unittest.mock.Mock(spec=self.media_cog._delete_mediaalias)
        self.media_cog._delete_mediaalias.side_effect = EkkoNonexistentAlias('testalias1')
        self.media_cog.cmd_mediaalias_delete(ts3_event)
        self.media_cog.ekko_bot.reply.assert_called_once_with('no such alias available.', ts3_event)

    def test_mediaalias_delete_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_mediaalias_delete(self.unsuited_ts3_event)


class MediacogPlaybackTest(MediacogTestCase):
    def setUp(self):
        super().setUp()

    def test_skip_continue(self):
        ts3_event = self._create_ts3_event(msg=b'!skip')
        self.media_cog.mpv.playlist = [1, 2]
        self.media_cog.mpv.playlist_pos = 0
        self.media_cog.cmd_skip(ts3_event)
        self.assertEqual(self.media_cog.mpv.playlist_pos, 1)

    def test_skip_stop(self):
        ts3_event = self._create_ts3_event(msg=b'!skip\\s5')
        self.media_cog.mpv.playlist = [1, 2]
        self.media_cog.mpv.playlist_pos = 0
        self.media_cog.cmd_skip(ts3_event)
        self.media_cog.mpv.command.assert_called_once_with('stop')

    def test_skip_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_skip(self.unsuited_ts3_event)

    def test_pausemedia(self):
        ts3_event = self._create_ts3_event(msg=b'!pausemedia')
        self.media_cog.cmd_pausemedia(ts3_event)
        self.assertTrue(self.media_cog.mpv.pause)
        self.ekko_bot.reply.assert_called_once_with('Playback paused. Resume with "!resumemedia".', ts3_event)

    def test_pausemedia_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_pausemedia(self.unsuited_ts3_event)

    def test_resumemedia(self):
        ts3_event = self._create_ts3_event(msg=b'!resumemedia')
        self.media_cog.cmd_resumemedia(ts3_event)
        self.assertFalse(self.media_cog.mpv.pause)
        self.ekko_bot.reply.assert_called_once_with('Playback resumed. Pause with "!pausemedia".', ts3_event)

    def test_resumemedia_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_resumemedia(self.unsuited_ts3_event)

    def test_clearqueue(self):
        ts3_event = self._create_ts3_event(msg=b'!clearqueue')
        self.media_cog.cmd_clearqueue(ts3_event)
        self.media_cog.mpv.playlist_clear.assert_called_once_with()
        self.ekko_bot.reply.assert_called_once_with('Queue cleared.', ts3_event)

    def test_clearqueue_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_clearqueue(self.unsuited_ts3_event)

    def test_queue(self):
        ts3_event = self._create_ts3_event(msg=b'!queue\\s-p\\s4\\s[url]https://www.youtube.com/watch?v=dQw4w9WgXcQ[/url]')
        self.media_cog._remove_url_bbcode = unittest.mock.Mock(return_value='https://www.youtube.com/watch?v=dQw4w9WgXcQ')
        self.media_cog.playlist_append = unittest.mock.Mock()
        self.media_cog.cmd_queue(ts3_event)
        self.media_cog.playlist_append.assert_called_once_with('https://www.youtube.com/watch?v=dQw4w9WgXcQ', 4)

    def test_queue_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_queue(self.unsuited_ts3_event)

    def test_media_current(self):
        ts3_event = self._create_ts3_event(msg=b'!media')
        self.media_cog._media_info_current = unittest.mock.Mock(return_value='media_info_return')
        self.media_cog.cmd_media(ts3_event)
        self.media_cog._media_info_current.assert_called_once_with()
        self.ekko_bot.reply.assert_called_once_with('media_info_return', ts3_event)

    def test_media_queue(self):
        ts3_event = self._create_ts3_event(msg=b'!media\\squeue')
        self.media_cog._media_info_queue = unittest.mock.Mock(return_value='media_info_return')
        self.media_cog.cmd_media(ts3_event)
        self.media_cog._media_info_queue.assert_called_once_with(5)
        self.ekko_bot.reply.assert_called_once_with('media_info_return', ts3_event)

    def test_media_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.media_cog.cmd_media(self.unsuited_ts3_event)


if __name__ == '__main__':
    unittest.main()
