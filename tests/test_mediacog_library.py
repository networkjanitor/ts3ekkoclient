import unittest
import unittest.mock
import mpv

from alchemy_mock.mocking import UnifiedAlchemyMagicMock, mock

from ts3ekkoclient.errors import EkkoNonexistentAlias
from ts3ekkoclient.cogs.media import MediaCog, TrackMeta
from ts3ekkoclient.bot import EkkoBot
from ts3ekkoclient.models import Invoker, MediaAlias
from ts3ekkoutil.envconsts import EkkoPropertyNames as epn


class MediacogLibBaseCase(unittest.TestCase):
    """
    Base test case for commonly used methods during the testing of the library methods of the media cog.
    """

    def custom_session_setup(self, dbsession):
        # Mock the parent EkkoBot of the text cog
        self.ekko_bot = unittest.mock.Mock(spec=EkkoBot)
        # Create some made up, but relevant args
        args = {
            epn.EKKO_MEDIA_DIRECTORY: '/mnt/media/',
            epn.COG_MEDIA_ALIAS_PREFIX: '\$',
            epn.COG_MEDIA_VOLUME_MAX: 100,
            epn.COG_MEDIA_VOLUME_MODIFIER: 0.2
        }
        # Create the to be tested media cog
        with unittest.mock.patch('mpv.MPV', unittest.mock.Mock(spec=mpv.MPV)):
            self.media_cog = MediaCog(self.ekko_bot, args, dbsession)


class MediacogMediaaliasLibTestGet(MediacogLibBaseCase):
    def test_mediaaliaslib_get(self):
        cmp_ma = MediaAlias(id=1, alias='testmediaalias1', value='urlurlurl')
        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(MediaAlias),
                 mock.call.filter_by(alias='testmediaalias1'),
                 mock.call.order_by(MediaAlias.id.desc())],
                [cmp_ma]
            )
        ])
        self.custom_session_setup(session)
        result_ma = self.media_cog._get_mediaalias('testmediaalias1')
        self.assertEqual(cmp_ma, result_ma)

    def test_mediaaliaslib_get_nonexistant(self):
        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(MediaAlias),
                 mock.call.filter_by(alias='testmediaalias1'),
                 mock.call.order_by(MediaAlias.id.desc())],
                [MediaAlias(id=1, alias='testmediaalias1', value='urlurlurl')]
            )
        ])
        self.custom_session_setup(session)
        with self.assertRaises(EkkoNonexistentAlias):
            self.media_cog._get_mediaalias('nonexistantalias')

    def test_mediaaliaslib_get_deleted(self):
        session = UnifiedAlchemyMagicMock(data=[
            (
                [mock.call.query(MediaAlias),
                 mock.call.filter_by(alias='testmediaalias1'),
                 mock.call.order_by(MediaAlias.id.desc())],
                [MediaAlias(id=2, alias='testmediaalias1', value=''),
                 MediaAlias(id=1, alias='testmediaalias1', value='urlurlurl')]
            )
        ])
        self.custom_session_setup(session)
        with self.assertRaises(EkkoNonexistentAlias):
            self.media_cog._get_mediaalias('testmediaalias1')


class MediacogMediaaliasLibTestSet(MediacogLibBaseCase):
    def test_mediaaliaslib_set(self):
        session = UnifiedAlchemyMagicMock()
        invoker = Invoker('z5TJ18g/dtkja4Xbrs9rOny4vl0=', 'Xyoz', 1)
        self.custom_session_setup(session)
        self.media_cog._set_mediaalias('testalias1', 'urlurlurl', invoker)
        session.add.assert_called_once()
        session.commit.assert_called_once()


class MediacogMediaaliasLibTestAppend(MediacogLibBaseCase):
    def setUp(self):
        session = UnifiedAlchemyMagicMock()
        self.invoker = Invoker('z5TJ18g/dtkja4Xbrs9rOny4vl0=', 'Xyoz',  1)
        self.custom_session_setup(session)
        self.media_cog._get_mediaalias = unittest.mock.Mock(spec=self.media_cog._get_mediaalias)

    def test_mediaaliaslib_append(self):
        self.media_cog._get_mediaalias.return_value = MediaAlias(id=1, alias='testmediaalias1', value='urlurlurl',
                                                                 invoker=self.invoker)
        self.media_cog._set_mediaalias = unittest.mock.Mock(spec=self.media_cog._set_mediaalias)
        self.media_cog._append_mediaalias('testalias1', 'appendurl', self.invoker)
        self.media_cog._set_mediaalias.assert_called_once_with('testalias1', 'urlurlurl appendurl', self.invoker)
        self.media_cog._get_mediaalias.assert_called_once_with('testalias1')

    def test_mediaaliaslib_append_nonexistant(self):
        self.media_cog._get_mediaalias.side_effect = EkkoNonexistentAlias('testalias1')
        self.media_cog._set_mediaalias = unittest.mock.Mock(spec=self.media_cog._set_mediaalias)
        self.media_cog._append_mediaalias('testalias1', 'appendurl', self.invoker)
        self.media_cog._set_mediaalias.assert_called_once_with('testalias1', 'appendurl', self.invoker)
        self.media_cog._get_mediaalias.assert_called_once_with('testalias1')


class MediacogMediaaliasLibTestDelete(MediacogLibBaseCase):
    def setUp(self):
        session = UnifiedAlchemyMagicMock()
        self.invoker = Invoker('z5TJ18g/dtkja4Xbrs9rOny4vl0=', 'Xyoz', 1)
        self.custom_session_setup(session)
        self.media_cog._get_mediaalias = unittest.mock.Mock(spec=self.media_cog._get_mediaalias)

    def test_mediaaliaslib_delete(self):
        self.media_cog._get_mediaalias.return_value = MediaAlias(id=1, alias='testmediaalias1', value='urlurlurl',
                                                                 invoker=self.invoker)
        self.media_cog._set_mediaalias = unittest.mock.Mock(spec=self.media_cog._set_mediaalias)
        self.media_cog._delete_mediaalias('testalias1', self.invoker)
        self.media_cog._set_mediaalias.assert_called_once_with('testalias1', None, self.invoker)
        self.media_cog._get_mediaalias.assert_called_once_with('testalias1')

    def test_mediaaliaslib_delete_nonexistant(self):
        self.media_cog._get_mediaalias.side_effect = EkkoNonexistentAlias('testalias1')
        self.media_cog._set_mediaalias = unittest.mock.Mock(spec=self.media_cog._set_mediaalias)
        self.media_cog._delete_mediaalias('testalias1', self.invoker)
        self.assertFalse(self.media_cog._set_mediaalias.called)
        self.media_cog._get_mediaalias.assert_called_once_with('testalias1')


class MediacogMediaaliasLibTestResolve(MediacogLibBaseCase):
    def setUp(self):
        session = UnifiedAlchemyMagicMock()
        self.custom_session_setup(session)
        self.media_cog._get_mediaalias = unittest.mock.Mock(spec=self.media_cog._get_mediaalias)

    def test_resolve_string_single(self):
        self.media_cog._get_mediaalias.return_value = MediaAlias(id=1, alias='testmediaalias1', value='urlurlurl')
        result = self.media_cog.mediaalias_resolve('$testmediaalias1')
        self.assertEqual(result, ['urlurlurl'])

    def test_resolve_string_multiple(self):
        self.media_cog._get_mediaalias.side_effect = [
            MediaAlias(id=1, alias='testmediaalias1', value='aliasvalue1'),
            MediaAlias(id=2, alias='testmediaalias2', value='aliasvalue21 aliasvalue22'),
            MediaAlias(id=3, alias='testmediaalias3', value='aliasvalue31 aliasvalue32 aliasvalue33')
        ]
        result = self.media_cog.mediaalias_resolve('$testmediaalias1 '
                                                   '$testmediaalias2 '
                                                   '$testmediaalias3 ')
        self.assertEqual(result, ['aliasvalue1', 'aliasvalue21', 'aliasvalue22', 'aliasvalue31',
                                  'aliasvalue32', 'aliasvalue33'])

    def test_resolve_list_single(self):
        self.media_cog._get_mediaalias.return_value = MediaAlias(id=1, alias='testmediaalias1', value='urlurlurl')
        result = self.media_cog.mediaalias_resolve(['$testmediaalias1'])
        self.assertEqual(result, ['urlurlurl'])

    def test_resolve_list_multiple(self):
        self.media_cog._get_mediaalias.side_effect = [
            MediaAlias(id=1, alias='testmediaalias1', value='aliasvalue1'),
            MediaAlias(id=2, alias='testmediaalias2', value='aliasvalue21 aliasvalue22'),
            MediaAlias(id=3, alias='testmediaalias3', value='aliasvalue31 aliasvalue32 aliasvalue33')
        ]
        result = self.media_cog.mediaalias_resolve(['$testmediaalias1',
                                                    '$testmediaalias2',
                                                    '$testmediaalias3'])
        self.assertEqual(result, ['aliasvalue1', 'aliasvalue21', 'aliasvalue22', 'aliasvalue31',
                                  'aliasvalue32', 'aliasvalue33'])


class MediacogMediaInfoLibTest(MediacogLibBaseCase):
    def setUp(self):
        session = UnifiedAlchemyMagicMock()
        self.custom_session_setup(session)

    def test_media_info_current(self):
        self.media_cog.mpv.playlist_pos = 0
        self.media_cog.mpv.playlist = [{'filename': 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'}]
        self.media_cog.mpv.playtime_remaining = 100
        self.media_cog.mpv.playback_time = 200
        with unittest.mock.patch('youtube_dl.YoutubeDL.extract_info',
                                 unittest.mock.Mock(return_value={'title': 'Title of Video'})):
            result = self.media_cog._media_info_current()
        self.assertEqual(result, '[url=https://www.youtube.com/watch?v=dQw4w9WgXcQ]Title of Video[/url] '
                                 '- 03:20 played, 01:40 remaining')

    def test_media_info_current_emptyqueue(self):
        self.media_cog.mpv.playlist_pos = None
        with unittest.mock.patch('youtube_dl.YoutubeDL.extract_info',
                                 unittest.mock.Mock(return_value={'title': 'Title of Video'})):
            result = self.media_cog._media_info_current()
        self.assertEqual(result, 'No track queued or playing!')

    def test_media_info_queue(self):
        self.media_cog.mpv.playlist_pos = 0
        self.media_cog.mpv.playlist = [{'filename': 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'},
                                       {'filename': 'https://www.youtube.com/watch?v=oHg5SJYRHA0'}]
        with unittest.mock.patch('youtube_dl.YoutubeDL.extract_info',
                                 unittest.mock.Mock(return_value={'title': 'Title of Video'})):
            result = self.media_cog._media_info_queue()
        self.assertEqual(result, 'Currently queued tracks:\n'
                                 '[url=https://www.youtube.com/watch?v=dQw4w9WgXcQ]Title of Video[/url]\n'
                                 '[url=https://www.youtube.com/watch?v=oHg5SJYRHA0]Title of Video[/url]')

    def test_media_info_queue_emptyqueue(self):
        self.media_cog.mpv.playlist_pos = None
        self.media_cog.mpv.playlist = None
        with unittest.mock.patch('youtube_dl.YoutubeDL.extract_info',
                                 unittest.mock.Mock(return_value={'title': 'Title of Video'})):
            result = self.media_cog._media_info_queue()
        self.assertEqual(result, 'Currently queued tracks:\nNo track queued or playing!')


class MediacogMediaAppendLibTest(MediacogLibBaseCase):
    def setUp(self):
        session = UnifiedAlchemyMagicMock()
        self.custom_session_setup(session)

    def test_playlist_append(self):
        clean_uri = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
        dirty_uri = [f'[url]{clean_uri}[/url]']
        self.media_cog.mediaalias_resolve = unittest.mock.Mock(return_value=dirty_uri)
        self.media_cog._remove_url_bbcode = unittest.mock.Mock(return_value=clean_uri)
        self.media_cog.mpv.playlist_pos = None

        self.media_cog.playlist_append(dirty_uri, None)

        self.media_cog.mpv.playlist_append.assert_called_once_with(clean_uri)
        self.assertEqual(self.media_cog.mpv.playlist_pos, 0)

    def test_playlist_append_pos(self):
        clean_uri = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
        dirty_uri = [f'[url]{clean_uri}[/url]']
        self.media_cog.mpv.playlist = [1, 2]
        self.media_cog.mpv.playlist_pos = 0
        self.media_cog.mediaalias_resolve = unittest.mock.Mock(return_value=dirty_uri)
        self.media_cog._remove_url_bbcode = unittest.mock.Mock(return_value=clean_uri)

        self.media_cog.playlist_append(dirty_uri, 3)

        self.media_cog.mpv.playlist_append.assert_called_once_with(clean_uri)
        self.media_cog.mpv.playlist_move.assert_called_once_with(1, 3)

    def test_playlist_append_pos_first(self):
        clean_uri = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
        dirty_uri = [f'[url]{clean_uri}[/url]']
        self.media_cog.mpv.playlist = [1, 2]
        self.media_cog.mpv.playlist_pos = 0
        self.media_cog.mediaalias_resolve = unittest.mock.Mock(return_value=dirty_uri)
        self.media_cog._remove_url_bbcode = unittest.mock.Mock(return_value=clean_uri)

        self.media_cog.playlist_append(dirty_uri, 0)

        self.media_cog.mpv.playlist_append.assert_called_once_with(clean_uri)
        self.media_cog.mpv.playlist_move.assert_called_once_with(1, 0)
        self.assertEqual(self.media_cog.mpv.playlist_pos, 0)


if __name__ == '__main__':
    unittest.main()
