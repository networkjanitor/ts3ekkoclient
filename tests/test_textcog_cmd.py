import unittest
import unittest.mock
import datetime

from ts3.response import TS3Event

from ts3ekkoclient.errors import EkkoUnsuitedCommand, EkkoNonexistentAlias, EkkoParsingError, EkkoArgparserMessage
from ts3ekkoclient.cogs.text import TextCog
from ts3ekkoclient.bot import EkkoBot
from ts3ekkoclient.models import Invoker, TextAlias


class TextcogTextaliasTestCase(unittest.TestCase):
    def setUp(self):
        # Let's create a single, uniform event for all unsuited-tests.
        self.unsuited_ts3_event = self._create_ts3_event(msg=b'!foobar')
        self.invoker = Invoker('z5TJ18g/dtkja4Xbrs9rOny4vl0=', 'Xyoz', 1)
        # Mock the parent EkkoBot of the text cog
        self.ekko_bot = unittest.mock.Mock(spec=EkkoBot)
        # Invoker determination returns prepared invoker
        self.ekko_bot.determine_invoker.return_value = self.invoker
        # Running with non-mocked command suitability
        self.ekko_bot.check_cmd_suitability = EkkoBot.check_cmd_suitability
        # Running with non-mocked parsing method (needs to be bound)
        self.ekko_bot.parse = EkkoBot.parse.__get__(self.ekko_bot, EkkoBot)
        # We dont want to have db access
        self.dbsession = unittest.mock.Mock()
        # Create the to be tested text cog
        self.text_cog = TextCog(self.ekko_bot, None, self.dbsession)

    def _create_ts3_event(self, msg=b'\\s', event_type=b'notifytextmessage', schandlerid=b'1', targetmode=b'2',
                          invokerid=b'1', invokername=b'Xyoz', invokeruid=b'z5TJ18g\\/dtkja4Xbrs9rOny4vl0='):
        """
        Shortcut to create TS3Events (mostly notifytextmessage) for testing purposes.

        For parameter documentation check TS3ClientQuery docs.

        :param event_type: ts3 event type (e.g. notifytextmessage)
        :return: TS3Event
        """
        creator_seq = b'%s schandlerid=%s targetmode=%s msg=%s invokerid=%s invokername=%s invokeruid=%s\n\r' % \
                      (event_type, schandlerid, targetmode, msg, invokerid, invokername, invokeruid)
        return TS3Event(creator_seq)

    def test_textalias_help(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias')
        self.text_cog.cmd_textalias(ts3_event)
        self.text_cog.ekko_bot \
            .reply.assert_called_once_with('usage: !textalias [set|get|delete|list-temporary|list-permanent]',
                                           ts3_event)


class TextcogTextaliasSet(TextcogTextaliasTestCase):
    def test_textalias_set_temporary(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sset\\stest0\\stest0\\svalue0')
        self.text_cog._add_alias = unittest.mock.Mock(spec=self.text_cog._add_alias)
        self.text_cog.cmd_textalias_set(ts3_event)
        self.text_cog._add_alias.assert_called_once_with('test0', 'test0 value0', self.invoker, False)

    def test_textalias_set_permanent(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sset\\s-p\\stest1\\stest1\\svalue1')

        self.text_cog._add_alias = unittest.mock.Mock(spec=self.text_cog._add_alias)
        self.text_cog.cmd_textalias_set(ts3_event)
        self.text_cog._add_alias.assert_called_once_with('test1', 'test1 value1', self.invoker, True)

    def test_textalias_set_empty_alias_empty_aliasvalue(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sset')

        self.text_cog._add_alias = unittest.mock.Mock(spec=self.text_cog._add_alias)
        with self.assertRaises(EkkoParsingError):
            self.text_cog.cmd_textalias_set(ts3_event)
        self.ekko_bot.reply.assert_called_once_with('usage: !textalias set [-h] [-p] aliasname [value [value ...]]\n'
                                                    '!textalias set: error: the following arguments are required: '
                                                    'aliasname, value\n', ts3_event)

    def test_textalias_set_empty_aliasvalue(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sset\\stest2')

        self.text_cog._add_alias = unittest.mock.Mock(spec=self.text_cog._add_alias)
        self.text_cog.cmd_textalias_set(ts3_event)
        self.text_cog._add_alias.assert_called_once_with('test2', '', self.invoker, False)

    def test_textalias_set_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.text_cog.cmd_textalias_set(self.unsuited_ts3_event)

    def test_textalias_set_shortform(self):
        ts3_event = self._create_ts3_event(msg=b'!~test0\\stest0\\svalue0')
        self.text_cog._add_alias = unittest.mock.Mock(spec=self.text_cog._add_alias)
        self.text_cog.cmd_textalias_set_shortform(ts3_event)
        self.text_cog._add_alias.assert_called_once_with('test0', 'test0 value0', self.invoker)

    def test_textalias_set_shortform_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.text_cog.cmd_textalias_set_shortform(self.unsuited_ts3_event)

    def test_cmd_textalias_set_help(self):
        """general test if help messages are raised"""
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sset\\s-h')
        with self.assertRaises(EkkoArgparserMessage):
            self.text_cog.cmd_textalias_set(ts3_event)


class TextcogTextaliasGet(TextcogTextaliasTestCase):
    def setUp(self):
        super().setUp()
        self.text_cog_get_alias = TextAlias(alias='test1', value='testvalue1',
                                            timestamp=datetime.datetime(2017, 12, 12, 3, 12, 5))
        self.text_cog.find_alias = unittest.mock.Mock(spec=self.text_cog.find_alias)
        self.text_cog.find_alias.return_value = self.text_cog_get_alias

    def test_textalias_get_temporary(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sget\\stest1')
        self.text_cog.cmd_textalias_get(ts3_event)
        self.text_cog.find_alias.assert_called_once_with('test1', False)
        self.text_cog.ekko_bot.reply.assert_called_once_with(self.text_cog_get_alias.value, ts3_event)

    def test_textalias_get_permanent(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sget\\s-p\\stest1')
        self.text_cog.cmd_textalias_get(ts3_event)
        self.text_cog.find_alias.assert_called_once_with('test1', True)
        self.text_cog.ekko_bot.reply.assert_called_once_with(self.text_cog_get_alias.value, ts3_event)

    def test_textalias_get_nonexistant(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sget\\stestnonexistant')
        self.text_cog.find_alias.side_effect = EkkoNonexistentAlias('testnonexistant')
        self.text_cog.cmd_textalias_get(ts3_event)
        self.text_cog.find_alias.assert_called_once_with('testnonexistant', False)
        self.text_cog.ekko_bot.reply.assert_called_once_with('no such alias available.', ts3_event)

    def test_textalias_get_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.text_cog.cmd_textalias_get(self.unsuited_ts3_event)

    def test_textalias_get_shortform(self):
        ts3_event = self._create_ts3_event(msg=b'!~test1')
        self.text_cog.cmd_textalias_get_shortform(ts3_event)
        self.text_cog.find_alias.assert_called_once_with('test1', permanent=False)
        self.text_cog.ekko_bot.reply.assert_called_once_with(self.text_cog_get_alias.value, ts3_event)

    def test_textalias_get_shortform_forcetemporary(self):
        ts3_event = self._create_ts3_event(msg=b'!~test1')
        self.text_cog.ekko_bot.can = unittest.mock.Mock(side_effect=[False, True])
        self.text_cog.cmd_textalias_get_shortform(ts3_event)
        self.text_cog.find_alias.assert_called_once_with('test1', permanent=False)
        self.text_cog.ekko_bot.reply.assert_called_once_with(self.text_cog_get_alias.value, ts3_event)

    def test_textalias_get_shortform_temporary_permanent(self):
        ts3_event = self._create_ts3_event(msg=b'!~test1')
        self.text_cog.find_alias = unittest.mock.Mock(
            side_effect=[EkkoNonexistentAlias('test1'), self.text_cog_get_alias])
        self.text_cog.cmd_textalias_get_shortform(ts3_event)
        self.text_cog.find_alias.assert_has_calls([unittest.mock.call('test1', permanent=False),
                                                   unittest.mock.call('test1', permanent=True)])
        self.text_cog.ekko_bot.reply.assert_called_once_with(self.text_cog_get_alias.value, ts3_event)

    def test_textalias_get_shortform_nonexistant(self):
        ts3_event = self._create_ts3_event(msg=b'!~testnonexistant')
        self.text_cog.find_alias.side_effect = EkkoNonexistentAlias('testnonexistant')
        self.text_cog.cmd_textalias_get_shortform(ts3_event)
        self.text_cog.find_alias.assert_has_calls([unittest.mock.call('testnonexistant', permanent=False),
                                                   unittest.mock.call('testnonexistant', permanent=True)])
        self.text_cog.ekko_bot.reply.assert_called_once_with('no such alias available.', ts3_event)

    def test_textalias_get_shortform_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.text_cog.cmd_textalias_get_shortform(self.unsuited_ts3_event)


class TextcogTextaliasDeleteTest(TextcogTextaliasTestCase):
    def setUp(self):
        super().setUp()

    def test_textalias_delete_temporary(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sdelete\\stestalias1')
        self.text_cog.delete_alias = unittest.mock.Mock(spec=self.text_cog.delete_alias)
        self.text_cog.cmd_textalias_delete(ts3_event)
        self.text_cog.delete_alias.assert_called_once_with('testalias1', self.invoker, False)

    def test_textalias_delete_permanant(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sdelete\\s-p\\stestalias1')
        self.text_cog.delete_alias = unittest.mock.Mock(spec=self.text_cog.delete_alias)
        self.text_cog.cmd_textalias_delete(ts3_event)
        self.text_cog.delete_alias.assert_called_once_with('testalias1', self.invoker, True)

    def test_textalias_delete_nonexistant(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\sdelete\\stestalias1')
        self.text_cog.delete_alias = unittest.mock.Mock(spec=self.text_cog.delete_alias,
                                                        side_effect=EkkoNonexistentAlias('testalias1'))
        self.text_cog.cmd_textalias_delete(ts3_event)
        self.ekko_bot.reply.assert_called_once_with('no such alias available for deletion.', ts3_event)

    def test_textalias_delete_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.text_cog.cmd_textalias_delete(self.unsuited_ts3_event)


class TextcogTextaliasListTest(TextcogTextaliasTestCase):
    def setUp(self):
        super().setUp()

    def test_textalias_list_temporary(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\slist-temporary')
        self.text_cog._list_temporary_alias = unittest.mock.Mock(
            spec=self.text_cog._list_temporary_alias, return_value='list_temporary_return_value')
        self.text_cog.cmd_textalias_list_temporary(ts3_event)
        self.ekko_bot.reply.assert_called_once_with('list_temporary_return_value', ts3_event)

    def test_textalias_list_temporary_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.text_cog.cmd_textalias_list_temporary(self.unsuited_ts3_event)

    def test_textalias_list_permanent(self):
        ts3_event = self._create_ts3_event(msg=b'!textalias\\slist-permanent')
        self.text_cog._list_permanent_alias = unittest.mock.Mock(
            spec=self.text_cog._list_permanent_alias, return_value='list_permanent_return_value')
        self.text_cog.cmd_textalias_list_permanent(ts3_event)
        self.ekko_bot.reply.assert_called_once_with('list_permanent_return_value', ts3_event)

    def test_textalias_list_permanent_unsuited(self):
        with self.assertRaises(EkkoUnsuitedCommand):
            self.text_cog.cmd_textalias_list_permanent(self.unsuited_ts3_event)


if __name__ == '__main__':
    unittest.main()
