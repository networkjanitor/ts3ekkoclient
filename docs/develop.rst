Developer Scratchpad
====================

commands
--------

build & upload pip package: ``python3 setup.py build sdist bdist_wheel upload``

build and upload docker image: ``docker build . -t katakowl/ts3ekkoclient && docker push katakowl/ts3ekkoclient``



ts3ekko method-command structure
--------------------------------

.. code-block:: python

      # function takes (besides self) exactly one parameter: the event to be handled
      def mediaalias_get(self, event):
        cmd_prefix = '!mediaalias get'  # unique prefix which decides if this method should handle the given event

        # next step is to do the actual prefix checking
        # if this fails, an EkkoUnsuitedCommand error is raised and will be handled by the event handler,
        # which will then continue to check other commands
        if self.ekko_bot.check_cmd_suitability(f'^{cmd_prefix}', event[0]['msg']):
            # afterwards we check if the required permissions are available to the invoker
            # if this fails an PermissionDenied error is raised and will be handled by the event handler,
            # which in turn will reply to the invoker with a generic "permission denied, you require permission X" message.
            if self.ekko_bot.can(MediaCogPermission.MEDIAALIAS_GET, event):
                # now good to go to actually dissect the command and parse the arguments
                # for this we use the parse() wrapper, which will not only handle parsing errors for us (EkkoParsingError),
                # but also does the reply work for usage and help messages (EkkoArgparserMessage).
                # Both errors are once again handled by the event handler which in turn stops the handling of the given event.
                aliasname = self.ekko_bot.parse(MediaAliasParser.parse_mediaalias_get, event,
                                                cmd_prefix, event[0]['msg'])
                # now that we have ensured that:
                #  -> this method should handle this command
                #  -> the user has the required permission for this command
                #  -> the command is not a help command and is also not malformed
                # we can do the actual command work and do whatever the command is designed for
                try:
                    ma = self._get_mediaalias(aliasname)
                    self.ekko_bot.reply(f'MediaAlias "{aliasname}" stands for: {ma.value}', event)
                except EkkoNonexistentAlias:
                    self.ekko_bot.reply('no such alias available.', event)

