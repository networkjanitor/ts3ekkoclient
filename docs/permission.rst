Permission
==========

This is an overview of the current implemented and used permissions in the EkkoBot.

Control Permission
------------------

.. list-table::
   :widths: 25 25 50
   :header-rows: 1

   * - Command
     - Permission
     - Info
   * - !spawn
     - control.spawn
     - spawn new bot instances
   * - !despawn
     - control.despawn
     - despawn bot instances
   * - !join
     - control.join
     - move bot instances to own channel
   * - !name
     - control.name
     - set usernames of bot instances

Permission Permission
---------------------

.. list-table::
   :widths: 25 25 50
   :header-rows: 1

   * - Command
     - Permission
     - Info
   * - !permission add
     - permission.add
     - create new permissions of any kind
   * - !permission get
     - permission.get
     - get all grants for any permission
   * - !permission delete
     - permission.delete
     - delete grants/denies of any kind
   * - !permission info
     - permission.info
     - get documentation for any permission

Textalias Permission
--------------------

.. list-table::
   :widths: 25 25 50
   :header-rows: 1

   * - Command
     - Permission
     - Info
   * - !textalias set
     - textalias.temporary.set
     - create new temporary textalias
   * - !textalias get
     - textalias.temporary.get
     - make bot post the content of any temporary textalias
   * - !textalias delete
     - textalias.temporary.delete
     - delete any temporary textalias
   * - !textalias list-temporary
     - textalias.temporary.list
     - list all temporary textalias

   * - !textalias set -p
     - textalias.permanent.set
     - create new permanent textalias
   * - !textalias get -p
     - textalias.permanent.get
     - make bot post the content of any permanent textalias
   * - !textalias delete -p
     - textalias.permanent.delete
     - delete any permanent textalias
   * - !textalias list-permanent
     - textalias.permanent.list
     - list all permanent textalias


Mediaalias Permission
---------------------

.. list-table::
   :widths: 25 25 50
   :header-rows: 1

   * - Command
     - Permission
     - Info
   * - !mediaalias set
     - media.alias.set
     - create new permanent mediaalias
   * - !mediaalias
     - media.alias.get
     - make bot post the content of any mediaalias
   * - !mediaalias delete
     - media.alias.delete
     - delete any mediaalias
   * - !mediaalias append
     - media.alias.append
     - append content to any mediaalias

Media Permission
----------------

.. list-table::
   :widths: 25 25 50
   :header-rows: 1

   * - Command
     - Permission
     - Info
   * - !queue
     - media.queue.append
     - append media to playback queue
   * - !skip
     - media.queue.skip
     - skip tracks in media queue
   * - !media
     - media.queue.media
     - query data about current playing track
   * - !media queue
     - media.queue.media_queue
     - query data about current playing and queued tracks
   * - !clearqueue
     - media.queue.clear
     - remove all tracks from media queue
   * - !pausemedia
     - media.pause
     - pause media playback
   * - !resumemedia
     - media.resume
     - resume media playback after pause
   * - !volume <volume>
     - media.volume.set
     - set the playback volume
   * - !volume
     - media.volume.get
     - get the playback volume
   * - !volume reset
     - media.volume.reset
     - reset the playback volume to default value


Other Permission
----------------

.. list-table::
   :widths: 25 25 50
   :header-rows: 1

   * - Command
     - Permission
     - Info
   * - !whoami
     - other.whoami
     - receive technical information about your ts3 identity