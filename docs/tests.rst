Tests
=====

Current Coverage
----------------

- covers all commands

  - includes testing if command is unsuited - because otherwise one command could snatch all events from other commands unexpectedly

- covers library functions behind commands
- covers *some* utility functions
- does **not** cover event handling and interaction with TS3ClientQuery

  - didn't put time in figuring out async tests
  - should be manually tested (after related code change) by trying one event per probe result:

    - success: command handled successfully (``!help``)
    - unsuited: command handled successfully - but other matching commands are prioritised (fallback usages, e.g. ``!mediaalias``)
    - parsing error: malformed commands (``!whoami -unknownswitch``) should give back usage information
    - conn reset: gets fired usually on very first received event, if that goes through you are good to go ~
    - parsing abort: request help/usage information (``!whoami --help``)

- does **not** cover identity replacement/ts3 prep

  - test this by replacing an identity in the ``settings.db`` and trying to start a ts3 client with the modified ``settings.db``

    - if no corrupt message appears and the unique_id in the ts3client matches with the one on identity creation, then you *might* be good to go
    - replace procedure with as many identities as possibl
    - unreliable, because even the current replacement process only works with some identities (reason currently unknown)


Environment
-----------

Needs to have the system package ``mpv-dev`` or ``mpv-libs`` (depending on linux distribution) installed (because a mock'd mpv.MPV class still requires this).

Execution
---------

Just the normal way in the venv, nothing fancy: ``python3 -m unittest discover -s tests``

Coverage
--------

Install ``coverage.py`` with ``pip install coverage``. Run ``coverage -m unittest discover -s tests`` followed by either ``coverage report -m`` (console output) or ``coverage html`` (browser). If you chose the later one, you can open the ``htmlcov/index.html`` in your browser afterwards.