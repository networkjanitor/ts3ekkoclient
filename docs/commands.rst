Commands
========

This is an overview of the current implemented commands in the EkkoBot.

Control Commands
''''''''''''''''

.. list-table::
   :widths: 25 75
   :header-rows: 1

   * - Command
     - Info
   * - ``!spawn``
     - Spawn new client in current channel of invoker. Submitted via direct message to any other instance on the server.
   * - ``!despawn``
     - Despawns/kills the targeted instance/client.
   * - ``!join``
     - Moves target client to current channel of invoker.
   * - ``!name <name>``
     - Change name of the bot to the given name.

Permission Commands
'''''''''''''''''''

.. list-table::
   :widths: 25 75
   :header-rows: 1

   * - Command
     - Info
   * - ``!permission add [-d|--deny] [-i|--unique-id] [-c|--channel-group] [-s|--server-group] <permission name>``
     - Create grants for a given permission name, based on invoker identification data.
   * - ``!permission get <permission name>``
     - Lists all existing grants for the given permission name.
   * - ``!permission delete <grant id>``
     - Deletes a grant based on the given grant id (get id from ``!permission get``)
   * - ``!permission info <permission name>``
     - Shows description and documentation for the given permission name.

Textalias Commands
''''''''''''''''''

.. list-table::
   :widths: 25 75
   :header-rows: 1

   * - Command
     - Info
   * - ``!textalias set [-p|--permanent] <aliasname> <text>*``
     - Creates a new temporary textalias for this bot instance. If -p is specified, the alias is marked as permanent and stored in the database.
   * - ``!textalias get [-p|--permanent] <aliasname>``
     - Access the content of a temporary textalias for this bot instance. If -p is specified, the permanent version of the alias searched for instead.
   * - ``!textalias delete [-p|--permanent] <aliasname>``
     - Delete a temporary textalias for this bot instance. If -p is specified, delete the permanent version of the alias instead.
   * - ``!textalias list-permanent``
     - List all available, permanent textaliases.
   * - ``!textalias list-temporary``
     - List all available, temporary textaliases for this bot instance.
   * - ``!~<aliasname> <text>*``
     - Shortform of ``!textalias set <aliasname <text>*``
   * - ``!~<aliasname>``
     - Shortform of ``!textalias get <aliasname``

Audio/Media(alias) Commands
'''''''''''''''''''''''''''

.. list-table::
   :widths: 25 75
   :header-rows: 1

   * - Command
     - Info
   * - ``!queue [-p|--position=<pos>] <uri>*``
     - Appends all uris onto playlist (if specified: at the given position). All sources understood by mpv and youtube-dl are valid.
   * - ``!skip [<count>]``
     - Skips a number of tracks, starting with the current playing (default: 1)
   * - ``!media [queue]``
     - Shows information about the current playing track (uri, timestamp). If ``queue`` keyword specified, prints the remaining playlist uris instead.
   * - ``!mediaalias set <aliasname> <uri>*``
     - Creates an alias for one or media media uris. This allows to use the newly created alias in the ``!queue`` command instead of the listed uris. Prefix the alias with ``$`` when using it in ``!queue`` to mark it as media alias. The alias is global.
   * - ``!mediaalias get <aliasname>``
     - Shows information (content, creator) of the alias.
   * - ``!mediaalias delete <aliasname>``
     - Deletes the alias.
   * - ``!mediaalias append <aliasname> <uri>*``
     - Appends uris to the alias content.
   * - ``!pausemedia``
     - Pauses playback.
   * - ``!resumemedia``
     - Resumes playback.
   * - ``!clearqueue``
     - Deletes all tracks from queue.
   * - ``!volume reset``
     - Resets volume to default value.
   * - ``!volume <value>``
     - Sets volume.
   * - ``!volume``
     - Gets volume.
