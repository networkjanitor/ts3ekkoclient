Database Stuff
==============

Sequences are initialised at 1001 with an increment of 1. If your fixtures should not be re-inserted every tine the ts3ekkomanage gets restarted, then please configure them correctly.

The safest way for that is to define all your seed data with ids below 1000. This way they raise an expected IntegrityError on re-start and won't pollute your database. On the tables that include a UniqueConstraint on a column (which is not a Sequence) you can also ignore this.