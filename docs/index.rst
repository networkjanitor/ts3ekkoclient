.. ts3ekkoclient documentation master file, created by
   sphinx-quickstart on Sat Dec 23 02:48:16 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ts3ekkoclient's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   commands
   permission
   tests
   database
   develop
   graveyard_history



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
