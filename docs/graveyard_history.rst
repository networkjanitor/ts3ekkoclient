Things that didn't really work out
==================================

Identity Switch
---------------

database manipulation (protobuf, single record)
'''''''''''''''''''''''''''''''''''''''''''''''

* protobuf reader worked fine (for identity detection)
* protobuf writer did not really produce the wanted result
    * might be related to failed parsed unknown flags which I did not manage to correctly identify type-wise

* :strike:`client disregards modification because of now wrong checksum`
* :strike:`not aware of way to recalculate checksum`
* checksum is sha1 over concatenation of all protobuf objects (value column) in the ProtobufItems table, sorted by ascended keys

database manipulation (string replace in protobuf, single record)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

* replace the identity string with the new one, directly in the protobuf object
* replacement through string replacement, without interpreting or looking at other data inside the protobuf object
* identity switch is currently  implemented like that
* works in most cases

Problems:
`````````
* some weird shenanigans are going on when using the following identity: `1456449VJ/uT8HQ+GNkGK/R+uxmkRG24sB59NwNqVXkMXBN5W1sqVk5TXV9uNlBhDR1dNlRgNW1jGWoKXgkqX3Eie2l0Y3hQZQB3IUh8VSJ5C04lFywIWXxfDF9QGFNVDX95EnxSJ2JFZlZsYkhIQUlnY1dlNG1JTm43ZWYwZmZ5Q000anZwcWkvUjhja0RqQ0lpZUdJcTAzTC9RZz0=` (unique id: `B71fjfipTzu7QC2nMak1mILNfeE=`)
* TS3 client reports data corruption, offers only total deletion
* before deploying ts3ekko yourself, you should check if all your identities work as expected or if you run in the same error as described here.
* from my perspective not identifiable what causes this

qt secrets file manipulation (old client, 3.0.19)
'''''''''''''''''''''''''''''''''''''''''''''''''

* client crashes directly because who knows (core dumped according to log, but no crashdump/file log?)
    * feels like dead end, some weird shenanigans are going on here
* manipulation itself worked fine though
* not preferred because old client